"""The function(s) for the command 'abbreviator diff'."""
import abbrev_util as util
import sys

if "-n" not in sys.argv[1:] and "--no-color" not in sys.argv[1:]:
    NOT_IN_CHAPTER_COLOR = "\033[31m"
    NOT_IN_TEXT_COLOR = "\033[33m"
    CLEAR_COLOR = "\033[0m"
else:
    NOT_IN_CHAPTER_COLOR = NOT_IN_TEXT_COLOR = CLEAR_COLOR = ""


def abbreviation_diff(document_text: str):
    """Show a diff of abbreviations in the console.

    Args:
        text (str): The text to show the diff for.

    """
    text, abbr_chapter = util.split_abbr_chapter(document_text)

    chapter_abbreviations = util.parse_abbr_chapter(abbr_chapter)
    text_abbreviations = util.find_abbreviations(text)

    print(
        "Abbreviations that don't seem to occur in the text but are defined in the 'Abbreviations' chapter:"
    )

    not_in_chapter_found = False

    for chapter_abbreviation in chapter_abbreviations:
        if chapter_abbreviation not in text_abbreviations:
            print(
                "\t"
                + NOT_IN_TEXT_COLOR
                + chapter_abbreviation
                + ": \t"
                + chapter_abbreviations[chapter_abbreviation]
                + CLEAR_COLOR
            )
            not_in_chapter_found = True

    if not not_in_chapter_found:
        print("\tNone found")

    print()

    text_abbreviations = util.find_abbreviations(text)

    print("Abbreviations that were found in the text but aren't defined")

    not_in_text_found = False

    for text_abbreviation in text_abbreviations:
        if text_abbreviation not in chapter_abbreviations:
            print(
                "\t" + NOT_IN_CHAPTER_COLOR + text_abbreviation + CLEAR_COLOR
            )
            not_in_text_found = True

    if not not_in_text_found:
        print("\tNone found")


if __name__ == "__main__":
    with open("sample.txt", "r", encoding="utf-8") as sample_file:
        abbreviation_diff(sample_file.read())
