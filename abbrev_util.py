# -*- coding: utf-8 -*-

"""Utility functions for dealing with abbreviations."""

import os
import sys
import re
import json

if "-n" not in sys.argv[1:] and "--no-color" not in sys.argv[1:]:
    WARN = "\033[33mWARNING: "
    CLEAR = "\033[0m"
else:
    WARN = CLEAR = ""

ABBREVIATION_FILE_PATH = (
    os.path.dirname(__file__) + os.sep + "known_abbreviations.json"
)

MIN_ABBREVIATION_LENGTH = 2
MAX_ABBREVIATION_LENGTH = (
    4  # Note that abbreviations with hyphens can be double in length
)

ABBREVIATION_CHAPTER_HEADING_PATTERN = re.compile("\n[0-9.]+\tAbbreviations\n")
CHAPTER_HEADING_PATTERN = re.compile("\n(?=[0-9.]+\t[^$]{2,}\n)")

DEFINITION_PATTERN = r"{} \(({})\)"
DEFINITION_PATTERN_REVERSE = r"({}) \({}\)"
DEFINITION_WORD_PATTERN = "[a-z -]+"

QUANTIFIER = (
    "{"
    + str(MIN_ABBREVIATION_LENGTH)
    + ","
    + str(MAX_ABBREVIATION_LENGTH)
    + "}"
)

REGULAR_ABBR_PATTERN = rf"\b[A-Z]{QUANTIFIER}(?=s?[\s)])"
DASH_ABBR_PATTERN = rf"\b[A-Z]{QUANTIFIER}-[A-Z]{QUANTIFIER}(?=s?[\s)])"

ABBREVIATION_PATTERN = re.compile(
    f"({REGULAR_ABBR_PATTERN}|{DASH_ABBR_PATTERN})"
)


def print_utf8(*text: str, delim: str = " "):
    """
    Print a text with UTF-8 encoding, regardless of console encoding.

    Args:
        *text (str): The texts to print
        delim (str, optional): The texts to print (defaults to " ")

    """
    sys.stdout.buffer.write(
        (delim.join([str(t) for t in text]) + "\n").encode("utf-8")
    )
    sys.stdout.flush()


def define(abbreviation: str):
    """Return all known definitions for an abbreviation.

    Args:
        abbreviation (str): The abbreviation to define

    Returns:
        tuple: A tuple of the possible definitions (str). May be empty.

    """
    return load_known_abbreviations().get(abbreviation.upper(), ())


def load_known_abbreviations():
    """
    Load known abbreviations from a file.

    The abbreviations should be of format
    {"FOO": {"Foo Object Order": 2, "Foo Ordering Offer": 4}}
    where FOO is the abbreviation and FOo Object Order and Foo Ordering Offer
    are potential definitions and 2 and 4 their respective usage counts.

    Returns:
        dict: All known abbreviations as described above

    Raises:
        IOException: If the file operation fails.
        ValueError: If the file contains invalid values.

    """
    try:
        if not os.path.isfile(ABBREVIATION_FILE_PATH):
            with open(ABBREVIATION_FILE_PATH, "w") as abbr_file:
                abbr_file.write("{}")

        with open(ABBREVIATION_FILE_PATH, "r") as abbr_file:
            abbrs = json.load(abbr_file)
    except json.JSONDecodeError:
        print_utf8(
            WARN
            + "The abbreviation file is invalid JSON. Defaulting to {}."
            + CLEAR
        )
        abbrs = {}

    if type(abbrs) != dict:
        print_utf8(
            WARN
            + "The abbreviation file is malformatted. Defaulting to {}."
            + CLEAR
        )
        abbrs = {}

    return abbrs


def save_abbreviation(abbr: str, definition: str):
    """
    Save that a definition was used to the abbreviation file.

    This will save the abbreviation if it doesn't exist yet,
    save the definition with usage count 0 if it doesn't exist yet
    and finally increase the usage count of the definition by 1

    Args:
        abbreviation (str): The abbreviation to save
        definition (str): The found definition

    Raises:
        IOException: If the file operation fails.
        ValueError: If the file contains invalid values.

    """
    abbrs = load_known_abbreviations()
    abbr = abbr.upper()

    abbrs[abbr] = abbrs.get(abbr, {})
    abbrs[abbr][definition] = abbrs[abbr].get(definition, 0) + 1

    with open(ABBREVIATION_FILE_PATH, "w") as abbr_file:
        json.dump(abbrs, abbr_file)


def split_abbr_chapter(text: str):
    r"""
    Split the text into the text and the abbreviation chapter.

    The abbreviation chapter should be indicated by a number,
    a tab character \t and "Abbreviations".

    Args:
        text (str): The text to analyze

    Returns:
        str: The text without the abbreviation chapter
        str: The abbreviation chapter (without heading)

    Raises:
        ValueError: If the text does not contain an abbreviation chapter

    """
    split_res = re.split(ABBREVIATION_CHAPTER_HEADING_PATTERN, text, 1)

    if len(split_res) != 2:
        raise ValueError("Text does not contain an abbreviation chapter")

    text_first_half, other_text = split_res

    abbreviation_chapter, text_second_half = re.split(
        CHAPTER_HEADING_PATTERN, other_text, 1
    )

    return (text_first_half + "\n" + text_second_half, abbreviation_chapter)


def find_abbreviations(text: str):
    r"""
    Find all abbreviation-like words in a text.

    Return all matches of the regex \b[A-Z]{2,4}(?=s?[\s)])
    or \b[A-Z]{2,4}-[A-Z]{2,4}(?=s?[\s)])

    Args:
        text (str): The text to search

    Returns:
        tuple: A tuple of abbreviations

    """
    abbrs = re.findall(ABBREVIATION_PATTERN, text)
    abbrs = sorted(tuple(set(abbrs)))
    return abbrs


def parse_abbr_chapter(chapter_text: str):
    """
    Parse an abbreviation chapter.

    Write all abbreviations to the abbreviations file; return a dict of them.

    Args:
        chapter_text (str): The abbreviations chapter

    Returns:
        dict: Abbreviations and their definitions

    """
    abbreviations = {}

    for line in chapter_text.split("\n"):
        if "\t" in line:
            abbreviation, definition = line.split("\t")
            save_abbreviation(abbreviation, definition)
            abbreviations[abbreviation] = definition

    return abbreviations


def find_text_definitions(text: str, abbreviations: list = None):
    """Find all potential definitions for a list of abbreviations or all.

    Does not write to the known abbreviations file
    If abbreviations is None, search for all found abbreviations.

    Args:
        text (str): The text to find definitions in
        abbreviations (list): The abbreviations (str) to define.
            Defaults to None.

    Returns:
        Found definitions in a dict: {"ABBR": ["definition", ...], ...}

    """
    if abbreviations is None:
        abbreviations = find_abbreviations(text)

    definitions = {}

    for abbreviation in abbreviations:
        def_text_pattern = (DEFINITION_WORD_PATTERN + " ").join(
            abbreviation
        ) + DEFINITION_WORD_PATTERN

        abbr_def_pattern = DEFINITION_PATTERN.format(
            abbreviation, def_text_pattern
        )
        abbr_def_pattern_reverse = DEFINITION_PATTERN_REVERSE.format(
            def_text_pattern, abbreviation,
        )

        abbr_definitions = [
            *re.findall(abbr_def_pattern, text),
            *re.findall(abbr_def_pattern_reverse, text),
        ]

        if len(abbr_definitions) != 0:
            definitions[abbreviation] = tuple(set(abbr_definitions))

    return definitions


if __name__ == "__main__":
    import sys

    with open("sample.txt", "r", encoding="utf-8") as sample_file:
        text, abbr_chapter = split_abbr_chapter(sample_file.read())

        print_utf8(find_text_definitions(text))
        print_utf8(parse_abbr_chapter(abbr_chapter))
        print_utf8(find_abbreviations(text))
