# abbreviator

A program for managing abbreviations in documents

## Usage

### Sub-commands

There are three sub-commands in abbreviator:

- `list` which lets you list all potential abbreviations in a document and
potential definitions
- `diff` which shows you all abbreviations that are used in a document but
aren't defined in the abbreviations chapter and vice versa
- `generate` which generates an abbreviation chapter out of the used
abbreviations, the existing abbreviations chapter and previously used
abbreviations.

Abbreviator does not support reading from .docx documents at the time.
Therefore, abbreviator reads from plain text (.txt) files. The easiest way to create one is pressing `Ctrl`+`A` in your document to select all text, then `Ctrl`+`c` to copy, and inserting that into a blank .txt file (you could use Notepad for that) **with encoding `UTF-8`**.

### Arguments

To read input from a file, use the `-f` option like this:

    abbreviator list -f sample.txt

    abbreviator diff -f sample.txt
    
    abbreviator generate -f sample.txt

Alternatively, you *could* input the text directly with `-t`, but you'd have to make sure that your shell treats that as one argument.

### Options

Every command supports the `-n` option to disable ANSI escapes. Do that if weird characters appear in the output, like: `[31m`

The commands `list` and `generate` also support the following options:
- `-i`, `--ignore-defs`: Ignore any found potential abbreviation definitions,
e.g. 'Foo Bar (FB)' or 'FB (Foo Bar)'
- `-I`, `--ignore-chapter`: Ignore existing abbreviations chapter

### Example

If you are using Bash (has ANSI escapes) and want to generate an abbreviations chapter while ignoring the previous abbreviations chapter, your command might look like this:

    abbreviator generate -f document.txt -I
