"""The function(s) for the command 'abbreviator generate'."""
import abbrev_util as util
import re
import sys


SEPARATOR = "=" * 8
INDENT = " " * 4

if "-n" not in sys.argv[1:] and "--no-color" not in sys.argv[1:]:
    ABBREVIATION_COLOR = "\033[1m"
    DEFINITION_COLOR = "\033[1m"
    CLEAR_COLOR = "\033[0m"
else:
    ABBREVIATION_COLOR = DEFINITION_COLOR = CLEAR_COLOR = ""

VALID_DEFINITION_PATTERN = re.compile(r"^[A-Z][a-z]+( [A-Z][a-z]*)*$")


def abbreviation_gen(
    document_text: str,
    ignore_chapter: bool = False,
    ignore_text_defs: bool = False,
):
    """Generate an abbreviation chapter with user input.

    Args:
        text (str): The text to generate an abbreviations chapter for.
        ignore_chapter (bool): Whether to ignore definitions in the
            abbreviations chapter. Defaults to False.
        ignore_text_defs (bool): Whether to ignore in-text definitions.
            Defaults to False.

    """
    text, abbr_chapter = util.split_abbr_chapter(document_text)

    if not ignore_chapter:
        util.parse_abbr_chapter(abbr_chapter)

    text_abbreviations = util.find_abbreviations(text)

    if not ignore_text_defs:
        text_definitions = util.find_text_definitions(text, text_abbreviations)
    else:
        text_definitions = {}

    chosen_definitions = {}

    abbrev_found = False

    for text_abbreviation in text_abbreviations:
        abbr_definitions = tuple(
            set().union(
                text_definitions.get(text_abbreviation, []),
                util.define(text_abbreviation),
            )
        )

        chosen_definition = abbreviation_chooser(
            text_abbreviation, abbr_definitions
        )

        if chosen_definition is not None:
            chosen_definitions[text_abbreviation] = chosen_definition
            util.save_abbreviation(text_abbreviation, chosen_definition)

        abbrev_found = True

    if not abbrev_found:
        util.print_utf8("No abbreviations found")

    util.print_utf8()
    util.print_utf8(
        "Abbreviation chapter (you can copy/paste this into your document)"
    )
    util.print_utf8(SEPARATOR)
    util.print_utf8()

    for abbreviation in chosen_definitions:
        util.print_utf8(
            abbreviation, chosen_definitions[abbreviation], delim="\t"
        )


def abbreviation_chooser(abbreviation: str, definitions: list):
    """Propmpt the user to pick a definition for the abbreviation.

    Args:
        abbreviation (str): The abbreviation to define
        definitions (list): Possible definitions

    Returns:
        str: The selected definition or None

    """
    util.print_utf8()
    util.print_utf8(
        "Found abbreviation: "
        + ABBREVIATION_COLOR
        + abbreviation
        + CLEAR_COLOR
    )

    util.print_utf8()

    if len(definitions) > 0:
        util.print_utf8(INDENT + "Could mean:")

        for definition_index in range(len(definitions)):
            util.print_utf8(
                INDENT * 2 + str(definition_index),
                DEFINITION_COLOR + definitions[definition_index] + CLEAR_COLOR,
                delim="\t",
            )

        util.print_utf8()

    user_choice_invalid = True
    while user_choice_invalid:
        if len(definitions) > 0:
            if len(definitions) > 1:
                util.print_utf8(
                    INDENT
                    + 'Type "n" to exclude the abbreviation, '
                    + "a number to use one of the found definitions "
                    + "or the definition for the abbreviation"
                )
                user_choice = input(
                    INDENT
                    + "(n / 0–"
                    + str(len(definitions) - 1)
                    + " / <Definition>): "
                )
            else:
                util.print_utf8(
                    INDENT
                    + 'Type "n" to exclude the abbreviation, '
                    + "0 to use the found definition "
                    + "or the definition for the abbreviation"
                )
                user_choice = input(INDENT + "(n / 0 / <Definition>): ")
        else:
            util.print_utf8(
                INDENT
                + 'Type "n" to exclude the abbreviation '
                + "or type the definition for the abbreviation"
            )
            user_choice = input(INDENT + "(n / <Definition>): ")

        if (
            user_choice.strip().lower() == "n"
            or re.match(VALID_DEFINITION_PATTERN, user_choice)
            or (
                user_choice.isnumeric() and int(user_choice) < len(definitions)
            )
        ):
            user_choice_invalid = False
        else:
            util.print_utf8(INDENT + "Invalid.")
            util.print_utf8()

    if user_choice.strip().lower() == "n":
        return None
    if user_choice.isnumeric() and int(user_choice) < len(definitions):
        return definitions[int(user_choice)]
    return user_choice.strip()


if __name__ == "__main__":
    with open("sample.txt", "r", encoding="utf-8") as sample_file:
        abbreviation_gen(sample_file.read())
