"""The function(s) for the command 'abbreviator list'."""
import abbrev_util as util
import sys

if "-n" not in sys.argv[1:] and "--no-color" not in sys.argv[1:]:
    HEADING_COLOR = "\033[1m"
    CLEAR_COLOR = "\033[0m"
else:
    HEADING_COLOR = CLEAR_COLOR = ""

COLUMN_SPACING = 2

ABBREVIATION_TITLE = "ABBR"
DEFINITION_TITLE = "Possible definition(s)"


def abbreviation_list(
    document_text: str,
    ignore_chapter: bool = False,
    ignore_text_defs: bool = False,
):
    """Show a list of found abbreviations in the console.

    Args:
        text (str): The text to show an abbreviation list for.
        ignore_chapter (bool): Whether to ignore definitions in the
            abbreviations chapter. Defaults to False.
        ignore_text_defs (bool): Whether to ignore in-text definitions.
            Defaults to False.

    """
    text, abbr_chapter = util.split_abbr_chapter(document_text)

    if not ignore_chapter:
        util.parse_abbr_chapter(abbr_chapter)

    text_abbreviations = util.find_abbreviations(text)

    abbrev_found = False

    max_abbr_length = len(ABBREVIATION_TITLE)

    for text_abbreviation in text_abbreviations:
        max_abbr_length = max(max_abbr_length, len(text_abbreviation))

    if not ignore_text_defs:
        text_definitions = util.find_text_definitions(text, text_abbreviations)
    else:
        text_definitions = {}

    print(
        HEADING_COLOR
        + ABBREVIATION_TITLE
        + (" " * (max_abbr_length - len(ABBREVIATION_TITLE) + COLUMN_SPACING))
        + DEFINITION_TITLE
        + CLEAR_COLOR
    )
    print()

    for text_abbreviation in text_abbreviations:
        abbr_definitions = set().union(
            text_definitions.get(text_abbreviation, []),
            util.define(text_abbreviation),
        )

        util.print_utf8(
            text_abbreviation
            + (
                " "
                * (max_abbr_length - len(text_abbreviation) + COLUMN_SPACING)
            )
            + "; ".join(abbr_definitions)
        )
        abbrev_found = True

    if not abbrev_found:
        print("None found")


if __name__ == "__main__":
    with open("sample.txt", "r", encoding="utf-8") as sample_file:
        abbreviation_list(sample_file.read())
